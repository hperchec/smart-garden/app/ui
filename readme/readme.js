/**
 * @hperchec/readme-generator Template EJS data example file
 */

'use strict'

// Dependencies
const markdownTable = require('markdown-table') // not 'require' here -> bug with markdown-table and node > 12.x.x
const fs = require('fs')
const path = require('path')
const GLOBALS = require('../globals.config.json')

// Based on the package.json file, get some data and informations
const packageJson = require('../package.json')
// Get dependencies
const dependencies = packageJson.dependencies
// Get dev dependencies
const devDependencies = packageJson.devDependencies
// Homepage
const homepage = packageJson.homepage
// Repository URL
const repositoryUrl = packageJson.repository.url
// Parent repository URL
const parentRepositoryUrl = 'https://gitlab.com/hperchec/smart-garden/app/root'
// Globals extracted source
const globalsSource = fs.readFileSync('./globals.config.json', { encoding: 'utf8' })
// Config extracted source
const configSource = fs.readFileSync('./src/config/config.js', { encoding: 'utf8' })
// Convert logo to image url
const logoPath = './public/favicon-16x16.png'
const logoImageUrl = `data:image/${path.extname(logoPath).slice(1)};base64,${fs.readFileSync(logoPath).toString('base64')}`

// Output a markdown formatted table from a js object
// Like:
// |name|version|
// |----|-------|
// |    |       |
function mdDependencies (deps) {
  return markdownTable([
    ['name', 'version'],
    ...(Object.entries(deps))
  ])
}

/**
 * Export data for readme file templating
 */
module.exports = {
  GLOBALS: GLOBALS,
  projectUrl: homepage,
  repositoryUrl: repositoryUrl,
  parentRepositoryUrl: parentRepositoryUrl,
  dependencies: mdDependencies(dependencies),
  devDependencies: mdDependencies(devDependencies),
  globalsSource: globalsSource,
  configSource: configSource,
  logoImageUrl: logoImageUrl
}