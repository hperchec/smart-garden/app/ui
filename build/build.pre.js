// Configuration
const CONFIG = require('../globals.config.json')
// App version
const APP_VERSION = CONFIG.VERSION.CURRENT

;(async () => {
  // Start
  console.log('Pre-build script: started...\n')

  console.log('App UI current version: ' + APP_VERSION + '\n')

  // End
  console.log('Pre-build script: exit')
})()
