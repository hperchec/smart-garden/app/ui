// Node v18 compat
const crypto = require('crypto')
const cryptoOrigCreateHash = crypto.createHash
crypto.createHash = algorithm => cryptoOrigCreateHash(algorithm === 'md4' ? 'sha256' : algorithm)

const path = require('path')
const webpack = require('webpack')
const StyleLintPlugin = require('stylelint-webpack-plugin')

const { APP_NAME, VERSION, SOCIAL, THEME } = require('./globals.config.json')

// Output path (relative to root directory)
const outputDir = 'www'
// JS output subdirectory (relative to output directory)
const jsOutputPath = 'js'
// CSS output subdirectory (relative to output directory)
const cssOutputPath = 'css'

// --

module.exports = {
  outputDir: outputDir,
  publicPath: './',
  devServer: {
    overlay: {
      warnings: true,
      errors: true
    }
  },
  configureWebpack: {
    // Output
    output: {
      filename:        `${jsOutputPath}/[name].${VERSION.CURRENT}.js`,
      chunkFilename:   `${jsOutputPath}/[name].${VERSION.CURRENT}.js`
    },
    resolve: {
      alias: {
        'vue$':      'vue/dist/vue.esm.js',
        '@':         path.resolve(__dirname, 'src'),
        '~':         path.resolve(__dirname, ''), // Root directory
        '#styles':   path.resolve(__dirname, 'src/scss'),
        '#mixins':   path.resolve(__dirname, 'src/mixins'),
        '#models':   path.resolve(__dirname, 'src/models'),
        '#core':     path.resolve(__dirname, 'src/core'),
        '#modules':  path.resolve(__dirname, 'src/core/modules'),
        '#services': path.resolve(__dirname, 'src/core/services'),
        '#tools':    path.resolve(__dirname, 'src/core/tools'),
        '#utils':    path.resolve(__dirname, 'src/core/utils')
      }
    },
    plugins: [
      // To define some global variables
      new webpack.DefinePlugin({
        '__UI_VERSION__':              `'${VERSION.CURRENT}'`,
        '__APP_NAME__':                `'${APP_NAME}'`,
        '__DISCORD_SERVER_ID__':       `'${SOCIAL.DISCORD_SERVER_ID}'`,
        '__DISCORD_INVITE_LINK__':     `'${SOCIAL.DISCORD_INVITE_LINK}'`,
        // Theme
        '__THEME_PRIMARY_COLOR__':     `'${THEME.__THEME_PRIMARY_COLOR__}'`,
        '__THEME_SECONDARY_COLOR__':   `'${THEME.__THEME_SECONDARY_COLOR__}'`,
        '__THEME_TERTIARY_COLOR__':    `'${THEME.__THEME_TERTIARY_COLOR__}'`,
        '__THEME_SUCCESS_COLOR__':     `'${THEME.__THEME_SUCCESS_COLOR__}'`,
        '__THEME_INFO_COLOR__':        `'${THEME.__THEME_INFO_COLOR__}'`,
        '__THEME_WARNING_COLOR__':     `'${THEME.__THEME_WARNING_COLOR__}'`,
        '__THEME_ERROR_COLOR__':       `'${THEME.__THEME_ERROR_COLOR__}'`,
        '__THEME_LIGHT_COLOR__':       `'${THEME.__THEME_LIGHT_COLOR__}'`,
        '__THEME_DARK_COLOR__':        `'${THEME.__THEME_DARK_COLOR__}'`,
        '__THEME_DARK_2_COLOR__':      `'${THEME.__THEME_DARK_2_COLOR__}'`,
        '__THEME_BREAKPOINTS_2XL__':   `${THEME.__THEME_BREAKPOINTS_2XL__}`,
        '__THEME_BREAKPOINTS_XL__':    `${THEME.__THEME_BREAKPOINTS_XL__}`,
        '__THEME_BREAKPOINTS_LG__':    `${THEME.__THEME_BREAKPOINTS_LG__}`,
        '__THEME_BREAKPOINTS_MD__':    `${THEME.__THEME_BREAKPOINTS_MD__}`,
        '__THEME_BREAKPOINTS_SM__':    `${THEME.__THEME_BREAKPOINTS_SM__}`
      }),
      // Scss linter
      new StyleLintPlugin({
        files: [
          'src/**/*.{vue,htm,html,css,sss,less,scss,sass}'
        ],
        exclude: [
          'node_modules',
          outputDir,
          'src/assets/**'
        ]
      })
    ]
  },
  chainWebpack: config => {
    if (process.env.NODE_ENV === 'production') {
      // config
      //   // Configure optimize-css plugin
      //   .plugin('optimize-css')
      //   .tap(([options]) => {
      //     // Default options
      //     options.cssnanoOptions.preset[1].mergeLonghand = false
      //     options.cssnanoOptions.preset[1].cssDeclarationSorter = false
      //     return [options]
      //   })
      config.plugins.delete('optimize-css')
    }
  },
  css: {
    extract: {
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename:         `${cssOutputPath}/[name].${VERSION.CURRENT}.css`,
      chunkFilename:    `${cssOutputPath}/[id].${VERSION.CURRENT}.css`
    },
    loaderOptions: {
      scss: {
        // To have access to variables in all CHILD SASS files & vue SFC
        prependData: `
          // Colors
          $__THEME_PRIMARY_COLOR__:      ${THEME.__THEME_PRIMARY_COLOR__};
          $__THEME_SECONDARY_COLOR__:    ${THEME.__THEME_SECONDARY_COLOR__};
          $__THEME_TERTIARY_COLOR__:     ${THEME.__THEME_TERTIARY_COLOR__};
          $__THEME_SUCCESS_COLOR__:      ${THEME.__THEME_SUCCESS_COLOR__};
          $__THEME_INFO_COLOR__:         ${THEME.__THEME_INFO_COLOR__};
          $__THEME_WARNING_COLOR__:      ${THEME.__THEME_WARNING_COLOR__};
          $__THEME_ERROR_COLOR__:        ${THEME.__THEME_ERROR_COLOR__};
          $__THEME_LIGHT_COLOR__:        ${THEME.__THEME_LIGHT_COLOR__};
          $__THEME_DARK_COLOR__:         ${THEME.__THEME_DARK_COLOR__};
          $__THEME_DARK_2_COLOR__:       ${THEME.__THEME_DARK_2_COLOR__};
          // Breakpoints
          $__THEME_BREAKPOINTS_2XL__:    ${THEME.__THEME_BREAKPOINTS_2XL__}px;
          $__THEME_BREAKPOINTS_XL__:     ${THEME.__THEME_BREAKPOINTS_XL__}px;
          $__THEME_BREAKPOINTS_LG__:     ${THEME.__THEME_BREAKPOINTS_LG__}px;
          $__THEME_BREAKPOINTS_MD__:     ${THEME.__THEME_BREAKPOINTS_MD__}px;
          $__THEME_BREAKPOINTS_SM__:     ${THEME.__THEME_BREAKPOINTS_SM__}px;
          // Variables file
          @import "~#styles/_variables.scss";
        `,
        sassOptions: {
          // Don't minify css (sass-loader)
          outputStyle: 'expanded'
        }
      }
    }
  }
}
