// Internationalization
import I18n from '@/i18n'
// Store
import Store from '@/store'
// Logger service
import Logger from '#services/logger'
// Helpers
import Helpers from '#services/helpers'

// Components
// import Home from '@/components/public/views/home/Index'
import About from '@/components/public/views/about/Index'
import Login from '@/components/public/views/login/Index'
import SignUp from '@/components/public/views/sign-up/Index'
import ForgotPassword from '@/components/public/views/forgot-password/Index'
import ResetPassword from '@/components/public/views/reset-password/Index'
import NotFound from '@/components/public/views/404/Index'

/**
 * vue-router public routes
 */
export default [
  /**
   * Home
   */
  {
    path: '/',
    name: 'Home',
    meta: {
      breadcrumb: {
        schema: null,
        title: (vm) => Helpers.capitalize(I18n.t('views.Home.title'))
      }
    },
    // redirect: 'Login' or 'Dashboard' if authenticated
    beforeEnter: (to, from, next) => {
      if (Store.state.Auth.currentUser) {
        next({
          path: '/dashboard'
        })
      } else {
        next({
          path: '/login'
        })
      }
    }
  },
  /**
   * About
   */
  {
    path: '/about',
    name: 'About',
    component: About,
    meta: {
      breadcrumb: {
        schema: ['[Home]', '<this>'],
        title: (vm) => Helpers.capitalize(I18n.t('views.About.title'))
      },
      pageMeta: {
        title: (vm) => Helpers.capitalize(I18n.t('views.About.title'))
      }
    }
  },
  /**
   * Login
   */
  {
    path: '/login',
    name: 'Login',
    component: Login,
    meta: {
      breadcrumb: {
        schema: ['[Home]', '<this>'],
        title: (vm) => Helpers.capitalize(I18n.t('user.login'))
      },
      pageMeta: {
        title: (vm) => Helpers.capitalize(I18n.t('user.login'))
      }
    },
    beforeEnter: async (to, from, next) => {
      if (Store.state.Auth.currentUser) {
        next({
          path: from.fullPath
        })
      } else {
        next()
      }
    }
  },
  /**
   * SignUp
   */
  {
    path: '/sign-up',
    name: 'SignUp',
    component: SignUp,
    meta: {
      breadcrumb: {
        schema: ['[Home]', '<this>'],
        title: (vm) => Helpers.capitalize(I18n.t('user.signUp'))
      },
      pageMeta: {
        title: (vm) => Helpers.capitalize(I18n.t('user.signUp'))
      }
    },
    beforeEnter: async (to, from, next) => {
      if (Store.state.Auth.currentUser) {
        next({
          path: from.fullPath
        })
      } else {
        next()
      }
    }
  },
  /**
   * Forgot password
   */
  {
    path: '/forgot-password',
    component: ForgotPassword,
    meta: {
      breadcrumb: {
        schema: ['[Home]', '<this>'],
        title: (vm) => Helpers.capitalize(I18n.t('views.ForgotPassword.title'))
      },
      pageMeta: {
        title: (vm) => Helpers.capitalize(I18n.t('views.ForgotPassword.title'))
      }
    },
    beforeEnter: async (to, from, next) => {
      if (Store.state.Auth.currentUser) {
        next({
          path: from.fullPath
        })
      } else {
        next()
      }
    }
  },
  /**
   * Reset password
   */
  {
    path: '/reset-password/:token',
    component: ResetPassword,
    meta: {
      breadcrumb: {
        schema: ['[Home]', '<this>'],
        title: (vm) => Helpers.capitalize(I18n.t('views.ResetPassword.title'))
      },
      pageMeta: {
        title: (vm) => Helpers.capitalize(I18n.t('views.ResetPassword.title'))
      }
    },
    // !! SEE ALSO -> ResetPassword component
    beforeEnter: async (to, from, next) => {
      if (Store.state.Auth.currentUser) {
        next({
          path: from.fullPath
        })
      } else {
        next()
      }
    }
  },
  /**
   * Catch all
   */
  {
    // will match everything
    path: '*',
    name: 'CatchAll',
    // redirect: '404',
    beforeEnter: (to, from, next) => {
      Logger.consoleLog('router', `❌ Not found route "${to.fullPath}". Redirect to "/404"`)
      next({
        path: '/404'
      })
    }
  },
  /**
   * 404
   */
  {
    path: '/404',
    name: '404',
    component: NotFound,
    meta: {
      pageMeta: {
        title: (vm) => Helpers.capitalize(I18n.t('global.pageNotFound'))
      }
    }
  }
]
