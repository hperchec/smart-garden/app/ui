/**
 * @vuepress
 * ---
 * title: log
 * headline: "Module: log"
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

/**
 * Module: log
 * @module log
 * @description
 * Module: log
 * ```javascript
 * // Import
 * import log from '#modules/log'
 * ```
 */

/**
 * log
 * @alias module:log
 * @description log method: shortcut for Logger.push()
 * @return {void}
 * @vuepress_syntax_block log
 * @vuepress_syntax_desc Access to log method, where `<args>` are Logger.push parameters
 * @vuepress_syntax_ctx {root}
 * this.log(...<args>)
 * @vuepress_syntax_ctx {component}
 * this.$root.log(...<args>)
 * @vuepress_syntax_ctx {any}
 * window.__ROOT_INSTANCE__.log(...<args>)
 */
export default function log (...args) {
  this.$Logger.consoleLog(...args) // Simple call of 'consoleLog' method...
}
