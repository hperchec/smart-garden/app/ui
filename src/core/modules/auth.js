/**
 * @vuepress
 * ---
 * title: auth
 * headline: "Module: auth"
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

/**
 * Module: auth
 * @module auth
 * @description
 * Module: auth
 * ```javascript
 * // Import
 * import auth from '#modules/auth'
 * ```
 */

/**
 * check
 * @alias module:auth.check
 * @description check method: shortcut for Store.Auth.actions.checkAuthenticated action
 * @return {boolean} See store Users module
 * @vuepress_syntax_block auth.check
 * @vuepress_syntax_desc Access to auth.check method
 * @vuepress_syntax_ctx {root}
 * this.auth.check()
 * @vuepress_syntax_ctx {component}
 * this.$root.auth.check()
 * @vuepress_syntax_ctx {any}
 * window.__ROOT_INSTANCE__.auth.check()
 */
function check () {
  return this.$store.dispatch('Auth/checkAuthenticated')
}

/**
 * isLoading
 * @alias module:auth.isLoading
 * @description isLoading method: shortcut for Store.state.Auth.currentUserLoading
 * @return {boolean} See store Auth module
 * @vuepress_syntax_block auth.isLoading
 * @vuepress_syntax_desc Access to auth.isLoading method
 * @vuepress_syntax_ctx {root}
 * this.auth.isLoading()
 * @vuepress_syntax_ctx {component}
 * this.$root.auth.isLoading()
 * @vuepress_syntax_ctx {any}
 * window.__ROOT_INSTANCE__.auth.isLoading()
 */
function isLoading () {
  return this.$store.state.Auth.currentUserLoading
}

/**
 * login
 * @alias module:auth.login
 * @description login method: shortcut for Store.Auth.actions.login (same parameters).
 * @return {(boolean|Error)} See store Auth module
 * @vuepress_syntax_block auth.login
 * @vuepress_syntax_desc Access to auth.login method, where `<args>` are Store.Auth.actions.login parameters
 * @vuepress_syntax_ctx {root}
 * this.auth.login(...<args>)
 * @vuepress_syntax_ctx {component}
 * this.$root.auth.login(...<args>)
 * @vuepress_syntax_ctx {any}
 * window.__ROOT_INSTANCE__.auth.login(...<args>)
 */
function login (...args) {
  return this.$store.dispatch('Auth/login', ...args)
}

/**
 * logout
 * @alias module:auth.logout
 * @description logout method
 * @return {void}
 * @vuepress_syntax_block auth.logout
 * @vuepress_syntax_desc Access to auth.logout method
 * @vuepress_syntax_ctx {root}
 * this.auth.logout()
 * @vuepress_syntax_ctx {component}
 * this.$root.auth.logout()
 * @vuepress_syntax_ctx {any}
 * window.__ROOT_INSTANCE__.auth.logout()
 */
async function logout () {
  await this.$store.dispatch('Auth/logout')
  // Reload page
  // this.$router.push('/login')
  window.location = '/'
}

/**
 * user
 * @alias module:auth.user
 * @description user method: shortcut for Store.state.Auth.currentUser object
 * @return {User} See store Auth module
 * @vuepress_syntax_block auth.user
 * @vuepress_syntax_desc Access to auth.user method
 * @vuepress_syntax_ctx {root}
 * this.auth.user()
 * @vuepress_syntax_ctx {component}
 * this.$root.auth.user()
 * @vuepress_syntax_ctx {any}
 * window.__ROOT_INSTANCE__.auth.user()
 */
function user () {
  return this.$store.state.Auth.currentUser
}

/**
 * doesPasswordMatchPolicy
 * @alias module:auth.doesPasswordMatchPolicy
 * @description doesPasswordMatchPolicy method: check if passed string match password policy
 * @return {boolean}
 * @vuepress_syntax_block auth.doesPasswordMatchPolicy
 * @vuepress_syntax_desc Access to auth.doesPasswordMatchPolicy method where `<password>` is the string to check
 * @vuepress_syntax_ctx {root}
 * this.auth.doesPasswordMatchPolicy(<password>)
 * @vuepress_syntax_ctx {component}
 * this.$root.auth.doesPasswordMatchPolicy(<password>)
 * @vuepress_syntax_ctx {any}
 * window.__ROOT_INSTANCE__.auth.doesPasswordMatchPolicy(<password>)
 */
function doesPasswordMatchPolicy (password) {
  // Define here the app policy for password
  // Minimum: 10 characters
  const lengthPasses = password.length >= 10
  // At least 1 upper case
  const lowercasePasses = password.toUpperCase() !== password
  // At least 1 lower case
  const uppercasePasses = password.toLowerCase() !== password
  // At least 1 number
  const numericPasses = /[0-9]/.test(password)
  // At least 1 special character
  const specialCharacterPasses = /[^A-Za-z0-9]/.test(password)

  return lengthPasses &&
    lowercasePasses &&
    uppercasePasses &&
    numericPasses &&
    specialCharacterPasses
}

// Object module
const auth = {
  check,
  isLoading,
  login,
  logout,
  user,
  doesPasswordMatchPolicy
}

/**
 * auth
 * @alias module:auth
 * @typicalname auth
 * @type {Object}
 * @constant
 * @description auth module
 * @vuepress_syntax_block auth
 * @vuepress_syntax_desc Access to auth module
 * @vuepress_syntax_ctx {root}
 * this.auth
 * @vuepress_syntax_ctx {component}
 * this.$root.auth
 * @vuepress_syntax_ctx {any}
 * window.__ROOT_INSTANCE__.auth
 */
export default auth
