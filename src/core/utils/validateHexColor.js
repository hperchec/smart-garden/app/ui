/**
 * validateEmail
 * @description Return true if email is valid
 * @example
 * validateEmail('john.doe@example.com')
 * @param {string} value - The email to check
 * @return {boolean}
 */
const validateEmail = function (email) {
  return /^#[ABCDEFabcdef0123456789]{6}$/.test(email)
}

module.exports = validateEmail
