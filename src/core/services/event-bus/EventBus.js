// Classes
import ServiceAsPlugin from '#services/ServiceAsPlugin.js'

/**
 * EventBus class
 */
class EventBus extends ServiceAsPlugin {
  /**
   * Create a new instance
   */
  constructor () { // eslint-disable-line no-useless-constructor
    // Call ServiceAsPlugin constructor
    super()
  }

  /**
   * Install method for Vue
   * The service used as a plugin must absolutely overwrites this static method
   * @ignore
   * @param {Vue} _Vue - Vue
   * @param {Object} [options={}] - The options of the plugin
   */
  static install (_Vue, options = {}) {
    const Vue = _Vue
    // Define '$EventBus' for each Vue instance
    Vue.prototype.$EventBus = new Vue()
  }
}

export default EventBus
