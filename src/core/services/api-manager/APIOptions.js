/**
 * APIOptions class
 */
class APIOptions {
  /**
   * @private
   */
  _name; _driver; _options

  /**
   * Create new APIOptions
   * @param {string} name - The API name
   * @param {string} driver - The library to use in API.supportedDrivers
   * @param {Object} options - An object that the driver needs to configure itself
   */
  constructor (name, driver, options) {
    this.name = name
    this.driver = driver
    this.options = options
  }

  /**
   * API name
   * @category properties
   * @return {string} API name
   */
  get name () {
    return this._name
  }

  set name (value) {
    // Check type
    if (typeof value === 'string') {
      // Assign
      this._name = value
    } else {
      // Else -> throw error
      throw new Error('APIOptions class: \'name\' property must be String. ' + typeof value + ' received...')
    }
  }

  /**
   * API driver name
   * @category properties
   * @return {string} API driver name
   */
  get driver () {
    return this._driver
  }

  set driver (value) {
    // Check type
    if (typeof value === 'string') {
      // Assign
      this._driver = value
    } else {
      // Else -> throw error
      throw new Error('APIOptions class: \'driver\' property must be String. ' + typeof value + ' received...')
    }
  }

  /**
   * Driver options
   * @category properties
   * @return {Object} The driver options
   */
  get options () {
    return this._options
  }

  set options (value) {
    // Assign
    this._options = value // No control here...?
  }
}

export default APIOptions
