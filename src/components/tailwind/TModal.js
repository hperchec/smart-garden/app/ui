// Vue-tailwind component
import { TModal } from 'vue-tailwind/dist/components'

export default {
  component: TModal,
  props: {
    fixedClasses: {
      overlay: 'overflow-auto scrolling-touch left-0 top-0 bottom-0 right-0 w-full h-full fixed',
      wrapper: 'relative mx-auto',
      modal: 'overflow-visible relative ',
      close: 'flex items-center justify-center'
      // close: 'flex items-center justify-center rounded-full absolute right-0 top-0 -m-3 h-8 w-8 transition duration-100 ease-in-out focus:ring-2 focus:ring-blue-500 focus:outline-none focus:ring-opacity-50'
    },
    classes: {
      overlay: 'z-40 bg-black bg-opacity-80',
      wrapper: 'z-50 max-w-lg px-3 py-12',
      modal: 'bg-light dark:bg-dark shadow rounded',
      body: 'p-4',
      header: 'p-4 rounded-t',
      footer: 'p-4 rounded-b',
      close: 'bg-primary text-light rounded-full absolute right-0 top-0 -m-3 h-8 w-8 transition duration-100 ease-in-out hover:bg-primary-400 focus:bg-primary-400',
      closeIcon: 'fill-current dark:fill-dark h-4 w-4',
      overlayEnterClass: 'opacity-0',
      overlayEnterActiveClass: 'transition ease-out duration-100',
      overlayEnterToClass: 'opacity-100',
      overlayLeaveClass: 'opacity-100',
      overlayLeaveActiveClass: 'transition ease-in duration-75',
      overlayLeaveToClass: 'opacity-0',
      enterClass: '',
      enterActiveClass: '',
      enterToClass: '',
      leaveClass: '',
      leaveActiveClass: '',
      leaveToClass: ''
    },
    variants: {
      danger: {
        overlay: 'bg-red-100',
        header: 'border-red-50 text-red-700',
        close: 'bg-red-50 text-red-700 hover:bg-red-200 border-red-100 border',
        modal: 'bg-white border border-red-100 shadow-lg',
        footer: 'bg-red-50'
      }
    }
  }
}
