// Vue-tailwind component
import { TSelect } from 'vue-tailwind/dist/components'

export default {
  component: TSelect,
  props: {
    fixedClasses: {
      wrapper: 'relative',
      input: `
        transition duration-200 ease-in-out
        focus:outline-none
        disabled:pointer-events-none
      `
    },
    classes: {
      input: `
        bg-transparent
        px-3 py-2
        rounded
        border-2 border-primary-200 dark:border-dark-2
        hover:border-primary dark:hover:border-primary
        focus:border-primary dark:focus:border-primary
        disabled:text-gray
        placeholder-gray
      `,
      arrowWrapper: 'pointer-events-none absolute inset-y-0 right-0 flex items-center px-2',
      arrow: 'fill-current h-4 w-4'
    },
    variants: {
      primary: `
        bg-transparent
        px-3 py-2
        rounded
        border-2 border-primary-200 dark:border-dark-2
        hover:border-primary dark:hover:border-primary
        focus:border-primary dark:focus:border-primary
        disabled:text-gray
      `,
      error: `
        bg-transparent
        px-3 py-2
        rounded
        border-2 border-error
        focus:border-error-600
        disabled:text-gray
      `
    }
  }
}
