// Vue-tailwind component
import { TInput } from 'vue-tailwind/dist/components'

export default {
  component: TInput,
  props: {
    fixedClasses: `
      transition duration-200 ease-in-out
      focus:outline-none
      disabled:pointer-events-none
    `,
    classes: `
      bg-transparent
      px-3 py-2
      rounded
      border-2 border-primary-200 dark:border-dark-2
      hover:border-primary dark:hover:border-primary
      focus:border-primary dark:focus:border-primary
      disabled:text-gray
    `,
    variants: {
      primary: `
        bg-transparent
        px-3 py-2
        rounded
        border-2 border-primary-200 dark:border-dark-2
        hover:border-primary dark:hover:border-primary
        focus:border-primary dark:focus:border-primary
        disabled:text-gray
      `,
      error: `
        bg-transparent
        px-3 py-2
        rounded
        border-2 border-error
        focus:border-error-600
        disabled:text-gray
      `
    }
  }
}
