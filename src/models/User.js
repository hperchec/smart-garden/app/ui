// utils
import { isDef } from '#utils'

const DEFAULTS = (prop) => User.defaults()[prop]

/**
 * User class
 */
class User {
  /**
   * @private
   */
  _id;
  _username;
  _role;
  _createdAt;
  _updatedAt;
  _lastLogin;

  /**
   * Create a User
   * @param {Object} data - Object that represents the user
   * @param {number} data.id - The unique user id
   * @param {string} data.username - The user username
   * @param {string} data.role - The user role
   * @param {?string} data.created_at - When the user has been created
   * @param {?string} data.updated_at - When the user has been updated
   * @param {?string} data.last_login - User last login date or null
   */
  constructor (data) {
    // If data is undefined -> called as `new User()`
    const __EMPTY__ = !isDef(data)
    data = __EMPTY__ ? DEFAULTS : data
    // Assign properties
    this._id = data.id
    this._username = data.username
    this._role = data.role
    this._createdAt = new Date(data.created_at)
    this._updatedAt = new Date(data.updated_at)
    this._lastLogin = data.last_login
  }

  /**
   * Accessors & mutators
   */

  /**
   * id
   * @category properties
   * @readonly
   * @description User id accessor
   * @type {number}
   */
  get id () {
    return this._id
  }

  /**
   * username
   * @category properties
   * @readonly
   * @description User username accessor
   * @type {string}
   */
  get username () {
    return this._username
  }

  /**
   * role
   * @category properties
   * @readonly
   * @description User role accessor
   * @type {string}
   */
  get role () {
    return this._role
  }

  /**
   * createdAt
   * @category properties
   * @readonly
   * @description User createdAt accessor
   * @type {Date}
   */
  get createdAt () {
    return this._createdAt
  }

  /**
   * updatedAt
   * @category properties
   * @readonly
   * @description User updatedAt accessor
   * @type {Date}
   */
  get updatedAt () {
    return this._updatedAt
  }

  /**
   * lastLogin
   * @category properties
   * @readonly
   * @description User lastLogin accessor
   * @type {Object[]}
   */
  get lastLogin () {
    return this._lastLogin
  }

  /**
   * Methods
   */

  /**
   * isAdmin
   * @category methods
   * @description Return true if user is admin
   * @return {boolean}
   */
  isAdmin () {
    return this.role === 'admin'
  }

  /**
   * Defaults
   */

  /**
   * Default properties for a new empty 'User' object
   * @return {Object} Default properties object
   */
  static defaults () {
    return {
      id: null,
      username: null,
      role: 'user',
      created_at: null,
      updated_at: null
    }
  }
}

export default User
