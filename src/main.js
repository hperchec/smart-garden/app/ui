/*
  Root app script
*/

// First: bootstrap.js
import './bootstrap.js'

// Vue
import Vue from 'vue'
// iso-i18n
import languages from '@cospired/i18n-iso-languages'
// Tailwind resolveConfig
import resolveTailwindConfig from 'tailwindcss/resolveConfig'
// Tailwind util -> flattenColorPalette
import flattenColorPalette from 'tailwindcss/lib/util/flattenColorPalette'

// Globals
import GLOBALS from '~/globals.config.json'
// Config
import CONFIG from '@/config/config.js'
// Tailwind config
import TAILWIND_CONFIG from '~/tailwind.config.js'

// Boot
import boot from '#core/boot'
// Init function
import init from '#core/init'

// Store
import Store from '@/store'
// Router
import Router from '@/router'
// I18n
import I18n from '@/i18n'

// // Services (reactive plugins)
// APIManager service
import APIManager from '#services/api-manager'

// Main App component
import App from '@/components/App'

// Prepare root instance data
// see also src/mixins/GlobalMixin
const rootInstanceData = {
  ROOT_DATA: {
    GLOBALS: {
      __UI_VERSION__: GLOBALS.VERSION.CURRENT,
      __APP_NAME__: GLOBALS.APP_NAME,
      __DISCORD_SERVER_ID__: GLOBALS.SOCIAL.DISCORD_SERVER_ID,
      __DISCORD_INVITE_LINK__: GLOBALS.SOCIAL.DISCORD_INVITE_LINK,
      ...GLOBALS.THEME
    },
    CONFIG: CONFIG,
    LANGS: languages.langs().reduce((obj, lang) => {
      obj[lang] = {
        lang: lang,
        name: languages.getName(lang, lang)
      }
      return obj
    }, {}),
    TAILWIND: {
      config: resolveTailwindConfig(TAILWIND_CONFIG),
      getColor: function (name) {
        const namedColors = flattenColorPalette(this.config.theme.colors)
        return namedColors[name]
      }
    }
  },
  CURRENT_BREAKPOINT: null, // Default to null (see below in mounted hook)
  CURRENT_THEME: CONFIG.SYSTEM.DEFAULT_THEME, // Default to 'auto' (see below in mounted hook)
  DATA_READY: false
}

// Run
;(async () => {
  /**
   * Boot
   */
  await boot()

  /**
   * Vue root instance
   */
  window.__ROOT_INSTANCE__ = new Vue({
    // Name
    name: '__ROOT_INSTANCE__',
    // Main informations
    el: '#app',
    template: '<App/>',
    components: {
      App // Main App component
    },
    // Data for root instance
    data: function () {
      return rootInstanceData
    },
    // I18n
    i18n: I18n,
    // Store
    store: Store,
    // Router
    router: Router,
    // Services (Vue reactive plugins)
    APIManager: APIManager,
    // Initialize in 'beforeCreate' hook
    beforeCreate () {
      init.call(this)
    },
    // Created
    created () {
      this.log('system', 'Root Vue instance created ✔')
      // Set theme
      this.ui.setTheme(localStorage.theme)
      // Detect device
      const deviceType = this.device.deviceType() || 'browser'
      this.log('system', `Detected device: ${deviceType}`)
      // Breakpoint & window resize observer
      this.CURRENT_BREAKPOINT = this.ui.getBreakpointForWidth(window.innerWidth)
      window.addEventListener('resize', () => {
        if (this.CURRENT_BREAKPOINT.name !== this.ui.getBreakpointForWidth(window.innerWidth).name) {
          this.CURRENT_BREAKPOINT = this.ui.getBreakpointForWidth(window.innerWidth)
          this.emit('BREAKPOINT_CHANGED', this.CURRENT_BREAKPOINT)
        }
      })
      this.log('system', 'Window resize event listener initialized ✔')
    },
    // Mounted
    async mounted () {
      // Load app necessary data
      if (this.auth.user()) { // If authenticated
        // Load circuits
        await this.$store.dispatch('Circuits/loadCircuits')
        // Loop on circuits
        for (const circuit of this.$store.state.Circuits.circuits) {
          // Get & set circuit status
          this.$store.dispatch('Circuits/getCircuitStatus', circuit.id)
        }
      }
      this.DATA_READY = true
    }
  })
})()
